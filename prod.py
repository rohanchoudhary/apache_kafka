from time import sleep
from json import dumps,dump
from kafka import KafkaProducer
import cv2
import numpy as np

producer = KafkaProducer(bootstrap_servers=['localhost:9092'])

vid = cv2.VideoCapture('test.mp4')			#input_video_location

i=0
while(True):
	ret, frame = vid.read()
	image = cv2.imencode('.jpg',frame)[1].tostring()						
	producer.send('numtest',value=image)		
	i+=1
	print(" Sent frame ", i)
vid.release()
